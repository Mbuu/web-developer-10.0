$(document).ready(function () {
    $('.popup-btn').on('click', function (e) {
        e.preventDefault();
        $('.popup').fadeIn();
    });

    $('.popup-close').click(function (e) {
        e.preventDefault();
        $('.popup').fadeOut();
    });

    $('.feedback-slider').slick({
        infinite: false,
        swipe: false
    });

    $('.features-slider').slick({
        infinite: true,
        slidesToShow: 4,
        slidesToScroll: 1,
        responsive: [
            {
                breakpoint: 992,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                    prevArrow: '<i class="material-icons features-arrow">keyboard_arrow_left</i>',
                    nextArrow: '<i class="material-icons features-arrow">keyboard_arrow_right</i>'
                }
            },
            {
                breakpoint: 767,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    prevArrow: '<i class="material-icons features-arrow">keyboard_arrow_left</i>',
                    nextArrow: '<i class="material-icons features-arrow">keyboard_arrow_right</i>'
                }
            }
        ]
    });
});